package main

import (
    _ "github.com/mattn/go-sqlite3"
    "database/sql"
    "fmt"
    svg "gitlab.com/beoran/svgo"
    "os"
    "strings"
    "strconv"
)

type Drakon struct {
    * sql.DB
    font Font
}


type Diagram struct {
    diagram_id int64
    name string
    origin string
    description sql.NullString
    zoom float64
    ox float64
    oy float64
}

type Item struct {
    item_id int64
    diagram_id int64
    type_ string
    text sql.NullString
    selected int
    x int
    y int
    w int
    h int
    a int
    b int
    aux_value sql.NullInt64
    color sql.NullString
    format sql.NullString
    text2 sql.NullString
    skipGenericText bool
}


func (item Item) X() int { return item.x; }
func (item Item) Y() int { return item.y; }
func (item Item) W() int { return item.w; }
func (item Item) H() int { return item.h; }
func (item Item) A() int { return item.a; }
func (item Item) B() int { return item.b; }


func (item Item) String() string {
    return fmt.Sprintf("item: %d, diagram: %d, type: %s, x: %d, y: %d, w: %d, h: %d, a: %d, b: %d",
        item.item_id, item.diagram_id, item.type_, item.x, item.y, item.w, item.h, item.a, item.b);
}


func DrakonOpen(filename string) (* Drakon, error) {
    db, err := sql.Open("sqlite3", filename)
    if err != nil { 
        return nil, err    
    }
    dra := &Drakon{}
    dra.DB      = db
    dra.font    = MONOSPACE_FONT
    return dra, nil
}

type RowsHandler func(rows * sql.Rows) error

func DoQueryMany(db * sql.DB, handler RowsHandler, query string, args ... interface {}) error {
    rows, err := db.Query(query, args...)
    if err != nil {
        return err
    }
        
    defer rows.Close()
    
    for rows.Next() {
        err := handler(rows)
        if err != nil {
            return err
        }
    }
    
    err = rows.Err() // get any error encountered during iteration
    
    if err != nil {
            return err
    }
    
    return nil
}

func (dra * Drakon) Diagrams() ([]Diagram, error) {
    res := make([]Diagram, 0)
    
    q := "select diagram_id, name, origin, description, zoom from diagrams;"
    
    handler := func (rows * sql.Rows) error {
        var diagram Diagram
        err := rows.Scan(&diagram.diagram_id, &diagram.name, &diagram.origin,
            &diagram.description, &diagram.zoom)
        if err != nil {
            return err
        }
        parts      := strings.Split(diagram.origin, " ")
        
        if f, err := strconv.ParseFloat(parts[0], 64) ; err == nil {
            diagram.ox = f
        }

        if f, err := strconv.ParseFloat(parts[1], 64) ; err == nil {
            diagram.oy = f
        }
        
        res = append(res, diagram)
        return nil
    }
    
    err := DoQueryMany(dra.DB, handler, q)
    
    if err != nil {
        return nil, err
    }
    
    return res, nil
}

func (dra * Drakon) ItemsForDiagram(diagram Diagram) ([]Item, error) {
    res := make([]Item, 0)
        
    q := "select item_id, diagram_id, type, text, selected, x, y, w, h, a, b, aux_value, color, format, text2 from items where diagram_id = ? order by type desc;"
    
    handler := func (rows * sql.Rows) error {
        var item Item
        err := rows.Scan(&item.item_id, &item.diagram_id, &item.type_,
            &item.text, &item.selected, &item.x, &item.y, &item.w, &item.h,
            &item.a, &item.b, &item.aux_value, &item.color, &item.format,
            &item.text2)
        if err != nil {
            return err
        }
        
        item.x -= int(diagram.ox)
        item.y -= int(diagram.oy)
        item.skipGenericText = false
        
        res = append(res, item)
        return nil
    }
    
    err := DoQueryMany(dra.DB, handler, q, diagram.diagram_id)
    
    if err != nil {
        return nil, err
    }
    
    return res, nil
}



func (dra * Drakon) Close() {
    dra.DB.Close()
}

type SvgExporter struct {
    * svg.SVG
    Visio bool
}

func NewExporter() (*SvgExporter, error) {
    exp := &SvgExporter{}
    return exp, nil
}

func (item Item) GetOrigin() (ox int, oy int) {
    ox = item.x - item.w
    oy = item.y - item.h
    return ox, oy
} 

func (item Item) Wide() int {
    return item.w * 2
} 

func (item Item) High() int {
    return item.h * 2
}

func (item Item) LineWide() int {
    return item.w
} 

func (item Item) LineHigh() int {
    return item.h
}

func (item Item) GetSize() (ww int, hh int) {
    ww = item.Wide()
    hh = item.High()
    return ww, hh
} 


func (item Item) GetLineOrigin() (ox int, oy int) {
    ox = item.x
    oy = item.y
    return ox, oy
} 


func (exp * SvgExporter) ExportAction(dra * Drakon, diagram Diagram, item Item) {
    ox, oy := item.GetOrigin()
    exp.Rect(ox, oy, item.Wide(), item.High(), "fill:white;stroke:black")
}

func (exp * SvgExporter) ExportBeginEnd(dra * Drakon, diagram Diagram, item Item) {
    ox, oy := item.GetOrigin()
    exp.Roundrect(ox, oy, item.Wide(), item.High(), 7, 7, "fill:white;stroke:black")
}

func (exp * SvgExporter) ExportCommentIn(dra * Drakon, diagram Diagram, item Item) {
    ox, oy := item.GetOrigin()
    dc := 5
    exp.Rect(ox - dc, oy - dc, item.Wide() + 2 * dc, item.High() + 2 * dc, "fill:grey;stroke:black")
    exp.Roundrect(ox, oy, item.Wide(), item.High(), 14, 14, "fill:white;stroke:black")
}

func (exp * SvgExporter) DrawBubble(x, y, w, h, p int, style string) {
    px := [7]int { x - p    , x        ,  x, x + w , x + w, x     , x     }
    py := [7]int { y + p / 8, y + p / 2,  y, y     , y + h, y + h , y + p } 
    exp.Polygon(px[0:7], py[0:7], style)
}

func (exp * SvgExporter) ExportCommentOut(dra * Drakon, diagram Diagram, item Item) {
    ox, oy := item.GetOrigin()
    exp.DrawBubble(ox, oy, item.Wide() , item.High(), item.a, "fill:yellow;stroke:black")    
}


func (exp * SvgExporter) DrawHexagon(x, y, w, h, p int, style string) {
    px := [6]int { x + p, x + w - p, x + w    , x + w - p, x + p, x         }
    py := [6]int { y    , y        , y + h / 2, y + h    , y + h, y + h / 2 } 
    exp.Polygon(px[0:6], py[0:6], style)
}

func (exp * SvgExporter) ExportIf(dra * Drakon, diagram Diagram, item Item) {
    ox, oy := item.GetOrigin()
    w      := item.Wide()
    h      := item.High()
    xlinefrom   := ox + item.Wide()
    xlineto     := ox + item.Wide() + item.a             
    yline       := oy + item.High() / 2 
    p           := 5
    exp.Line(xlinefrom, yline, xlineto , yline, "stroke:black");
    exp.DrawHexagon(ox - p, oy, item.Wide() + p * 2, item.High(), p * 2, "fill:#ffffaa;stroke:black")
    t1 := "Yes"
    t2 := "No"
    if (item.b == 0) {
        t1, t2 = t2, t1
    }
    
    t1w := dra.font.Width(t1)
    
    exp.Text( ox + w / 2 - p - t1w, oy + h + dra.font.LineHeight()     , t1,  fmt.Sprintf("font-family:%s;font-size:%dpx;",dra.font.Name(), dra.font.Size()))
    exp.Text( ox + w  + p   , oy + h / 2 - dra.font.LineHeight() , t2,  fmt.Sprintf("font-family:%s;font-size:%dpx;",dra.font.Name(), dra.font.Size()))
}

func (exp * SvgExporter) ExportHorizontal(dra * Drakon, diagram Diagram, item Item) {
    ox, oy := item.GetLineOrigin()
    x1 := ox
    y1 := oy
    x2 := x1 + item.LineWide()
    y2 := y1 + item.LineHigh()
    exp.SVG.Line(x1, y1, x2, y2, "stroke:black")
}

func (exp * SvgExporter) ExportVertical(dra * Drakon, diagram Diagram, item Item) {
    ox, oy := item.GetLineOrigin()
    x1 := ox
    y1 := oy
    x2 := x1 + item.LineWide()
    y2 := y1 + item.LineHigh()
    exp.SVG.Line(x1, y1, x2, y2, "stroke:black")
}

func (exp * SvgExporter) ExportTextAt(dra * Drakon, x, y, w, h int, text string) {
    /* lines := FitText(dra.font, text, w) 
     * Apparently no line fitting is needed as DRAKON already inserts the 
     * newline in the text at the break. 
     **/
    lines := strings.Split(text, "\n")
    for i, l := range lines {
        lines[i] = l + "\n"
    }
    style := fmt.Sprintf("font-family:%s;font-size:%dpx;width:%dpx;height:%dpx;", dra.font.Name() , dra.font.Size(), w, h)
    yo    := 0    
    /* Compensate for visio if requested -- not needed anymore with visio-specific extensions */
    /*
    if exp.Visio {
        nl := len(lines) 
        yo  = (dra.font.LineHeight() * nl) / 2
    }
    */
    if (exp.Visio) { 
        exp.VisioTextRect(svg.Px(x + w / 2), svg.Px(y + h /2), svg.Px(w), svg.Px(h))
    }
    exp.Textlinesex(svg.Px(x), svg.Px(y + yo), lines, svg.Px(dra.font.LineHeight()), svg.Px(dra.font.LineHeight()), style)
} 


func (exp * SvgExporter) ExportFitText2(dra * Drakon, diagram Diagram, item Item) {
    if item.text2.Valid {
        ox, oy := item.GetOrigin()
        oy = oy - item.High()
        ox = ox + 20 
        y := oy + dra.font.LineHeight()
        x := ox + 10
        exp.ExportTextAt(dra, x, y, item.Wide() - 20, item.High(), item.text2.String)
    }
} 


func (exp * SvgExporter) DrawLeftArrow(x, y, w, h, p int, style string) {
    px := [5]int { x + p, x + w ,  x + w , x + p, x         }
    py := [5]int { y    , y     ,  y + h , y + h, y + h / 2 } 
    exp.Polygon(px[0:5], py[0:5], style)
}


func (exp * SvgExporter) DrawRightArrow(x, y, w, h, p int, style string) {
    px := [5]int { x , x + w - p, x + w    , x + w - p, x     }
    py := [5]int { y , y        , y + h / 2, y + h    , y + h } 
    exp.Polygon(px[0:5], py[0:5], style)
}


func (exp * SvgExporter) DrawParallelogram(x, y, w, h, p int, style string) {
    px := [4]int { x + p, x + w + p, x + w, x}
    py := [4]int { y    , y        , y + h, y + h} 
    exp.Polygon(px[0:4], py[0:4], style)
}

func (exp * SvgExporter) DrawTrapeziumDown(x, y, w, h, p int, style string) {
    px := [4]int { x - p, x + w + p, x + w, x}
    py := [4]int { y    , y        , y + h, y + h} 
    exp.Polygon(px[0:4], py[0:4], style)
}

func (exp * SvgExporter) DrawHexagonDown(x, y, w, h, p int, style string) {
    px := [6]int { x    , x + w    , x + w    , x + w - p, x + p, x         }
    py := [6]int { y    , y        , y + h - p, y + h    , y + h, y + h - p } 
    exp.Polygon(px[0:6], py[0:6], style)
}

func (exp * SvgExporter) DrawHexagonUp(x, y, w, h, p int, style string) {
    px := [6]int { x + p, x + w - p, x + w    , x + w , x , x        }
    py := [6]int { y    , y        , y + p    , y + h , y + h, y + p }
    exp.Polygon(px[0:6], py[0:6], style)
}




func (exp * SvgExporter) CalculateOutput(dra * Drakon, diagram Diagram, item * Item) {
    
}


func (exp * SvgExporter) ExportOutput(dra * Drakon, diagram Diagram, item * Item) {

    rect_x     := item.X() - item.W()
    rect_y     := item.Y() - item.H() + item.A()
    rect_w     := 2 * item.W() - 30
    rect_h     := 2 * item.H() - item.A()

    arro_x     := rect_x  + 10
    arro_y     := item.Y() - item.H()
    arro_w     := 2 * item.W() - 10
    arro_h     := item.A()

    txt1_x     := rect_x + 5
    txt1_y     := rect_y + 5
    txt1_w     := rect_w - 10
    txt1_h     := rect_h - 10

    txt2_x     := arro_x + 5
    txt2_y     := arro_y + 0
    txt2_w     := arro_w - 10
    txt2_h     := arro_h - 5
    lj         := 20

    
    
    exp.Rect(rect_x, rect_y, rect_w, rect_h, "fill:#aaffff;stroke:black")
    exp.DrawRightArrow(arro_x, arro_y, arro_w, arro_h, lj,  "fill:white;stroke:black")
    
        
    if item.text.Valid {
        exp.ExportTextAt(dra, txt1_x, txt1_y, txt1_h,  txt1_w, item.text.String)
        item.skipGenericText = true // avoid redraw by generic text draw.
    }
    
    if item.text2.Valid {
        exp.ExportTextAt(dra, txt2_x, txt2_y, txt2_h,  txt2_w,  item.text2.String)
    }
    
}


func (exp * SvgExporter) ExportInsertion(dra * Drakon, diagram Diagram, item Item) {
    exp.Group()
    ox, oy := item.GetOrigin()
    exp.Rect(ox - 10, oy, item.Wide() + 20, item.High(), "fill:white;stroke:black")
    exp.Rect(ox, oy, item.Wide(), item.High(), "fill:white;stroke:black")    
    exp.Gend()
}

func (exp * SvgExporter) ExportInput(dra * Drakon, diagram Diagram, item * Item) {

    ox, oy := item.GetOrigin()
    hh := item.High() / 2
    oy += hh
    lj := 20
    // rh := hh - item.a
    /* I don't know how drakon interprets x, y, h, w, and a, but 
     * what I'm doing here looks OK for MY flow charts.
     * This is a purely ad-hock solution that may not work 
     * in the generic case. Probably I should translate the layout algorithm from 
     * the DRAKON TCL source code to Go. 
     */
    oy += item.a / 4
    exp.Rect(ox, oy, item.Wide() , hh, "fill:cyan;stroke:black")
    // text needs to be shifted down.
    if item.text.Valid {
        exp.ExportTextAt(dra, ox + 10, oy, item.Wide() - 20, item.High(), item.text.String)
        item.skipGenericText = true // avoid redraw by generic text draw.
    }
    hh  = hh + item.a / 2

    oy = oy - hh
    ox = ox + 20 
    
    exp.DrawLeftArrow(ox + lj, oy,  item.Wide(), hh, lj,  "fill:white;stroke:black")
    
    ox = ox + 30
    if item.text2.Valid {
        exp.ExportTextAt(dra, ox, oy, item.Wide() - 30, item.High(), item.text2.String)
    }
}

func (exp * SvgExporter) ExportCase(dra * Drakon, diagram Diagram, item Item) {
    ox, oy := item.GetOrigin()
    exp.DrawTrapeziumDown(ox, oy, item.Wide(), item.High(), 10, "fill:#ffaaaa;stroke:black")    
}

func (exp * SvgExporter) ExportSelect(dra * Drakon, diagram Diagram, item Item) {
    ox, oy := item.GetOrigin()
    exp.DrawParallelogram(ox, oy, item.Wide(), item.High(), 10, "fill:#ffaaaa;stroke:black")
}

func (exp * SvgExporter) ExportLoopStart(dra * Drakon, diagram Diagram, item Item) {
    ox, oy := item.GetOrigin()
    exp.DrawHexagonUp(ox, oy, item.Wide(), item.High(), 10, "fill:#99ff99;stroke:black")
}

func (exp * SvgExporter) ExportLoopEnd(dra * Drakon, diagram Diagram, item Item) {
    ox, oy := item.GetOrigin()
    exp.DrawHexagonDown(ox, oy, item.Wide(), item.High(), 10, "fill:#99ff99;stroke:black")
}

func (exp * SvgExporter) ExportUnknown(dra * Drakon, diagram Diagram, item Item) {
    fmt.Printf("Unknown element! %s\n", item)
}

type Font interface {
    Size() int
    Width(line string) int;
    LineHeight() int;
    Name() string
}

type MonoFont struct {
    Size_       int
    CharWidth_  int
    LineHeight_ int
    Name_       string 
};

func (fnt MonoFont) Size() int {
    return fnt.Size_
}

func (fnt MonoFont) Width(line string) int {
    return fnt.CharWidth_ * len(line)
}

func (fnt MonoFont) LineHeight() int {
    return fnt.LineHeight_
}

func (fnt MonoFont) Name() string {
    return fnt.Name_
}

var MONOSPACE_FONT MonoFont = MonoFont{ 12, 10, 14, "monospace"}

func FitText(fnt Font, text string, bound_w int) ([]string) { 
    result := make([]string, 0); 
    lines := strings.Split(text, "\n")
    for _, line := range lines {
        result_line := ""
        line_width  := 0
        words := strings.Split(line, " ")
        var sword string
        for _, word := range words {
            sword = " " + word
            word_width := fnt.Width(sword);
            new_line_width := line_width + word_width
            if (new_line_width > bound_w) {
                result = append(result, result_line)
                result_line = word
                line_width = 0;
            } else {
                result_line = result_line + sword
                line_width  = new_line_width
            }
        }
        if (len(result_line) > 0) {
            result = append(result, result_line)
        }
    }
    return result
}

func (exp * SvgExporter) ExportFitText(dra * Drakon, diagram Diagram, item Item) {
    if item.text.Valid {
        ox, oy := item.GetOrigin()
        y := oy
        x := ox + 10
        exp.ExportTextAt(dra, x, y, item.Wide() - 20, item.High(), item.text.String)
    }
} 

func (exp * SvgExporter) ExportItem(dra * Drakon, diagram Diagram, item Item) {
    fmt.Printf("Exporting item %s\n", item.String())
    exp.Group()
        
    switch item.type_ {
        case "action":      exp.ExportAction(dra, diagram, item)
        case "beginend":    exp.ExportBeginEnd(dra, diagram, item)
        case "commentin":   exp.ExportCommentIn(dra, diagram, item)        
        case "commentout":  exp.ExportCommentOut(dra, diagram, item)        
        case "if":          exp.ExportIf(dra, diagram, item)
        case "horizontal":  exp.ExportHorizontal(dra, diagram, item)
        case "vertical":    exp.ExportVertical(dra, diagram, item)
        case "output":      exp.ExportOutput(dra, diagram, &item)
        case "input":       exp.ExportInput(dra, diagram, &item)
        case "insertion":   exp.ExportInsertion(dra, diagram, item)
        case "case":        exp.ExportCase(dra, diagram, item)
        case "select":      exp.ExportSelect(dra, diagram, item)
        case "loopstart":   exp.ExportLoopStart(dra, diagram, item)
        case "loopend":     exp.ExportLoopEnd(dra, diagram, item)
        default:            exp.ExportUnknown(dra, diagram, item)
    }
    
    if ! item.skipGenericText {
        exp.ExportFitText(dra, diagram, item)
    }
    exp.Gend()
}



func (exp * SvgExporter) ExportDiagram(dra * Drakon, diagram Diagram) {    
    nout := strings.Replace(diagram.name, " ", "_",-1) + ".svg"
    fmt.Printf("Diagram %s->%s\n", diagram.name, nout)
    
    if items, err := dra.ItemsForDiagram(diagram) ; err != nil {
        fmt.Printf("Error: %s\n", err.Error())
    } else if  fout, err := os.Create(nout) ; err != nil { 
        fmt.Printf("Error: %s\n", err.Error())
    } else {
        defer fout.Close()        
        exp.SVG = svg.New(fout)
        exp.SVG.Start(svg.Uu(1500), svg.Uu(1200))
        for i := range  items {
            item := items[i]
            exp.ExportItem(dra, diagram, item);
        }
        exp.SVG.End()
        if  err != nil { 
            fmt.Printf("Error: %s\n", err.Error())
        }
    }
}


func (exp * SvgExporter) Export(dra * Drakon) {
    fmt.Printf("Exporting...\n")
    diagrams, err := dra.Diagrams()
    if err == nil {
        for i := range diagrams {
            diagram := diagrams[i]
            exp.ExportDiagram(dra, diagram)
        }
    } else {
        fmt.Printf("Error: %s\n", err.Error())
    }
}


func ShowHelp() {
    fmt.Printf(`Usage: drn2svg input.drn
    Convert a DRAKON file to one or more svg files.
    `)
    os.Exit(1)
}

func main() {
    
    if len(os.Args) < 2 {
        ShowHelp()
    }
    
    fin:= os.Args[1]

    dra, err := DrakonOpen(fin)
    
    if err != nil  {
        fmt.Printf("drn2svg: error: %s", err.Error());
        os.Exit(2);
    }
    
    defer dra.Close() 

    
    exp, err := NewExporter()
    if len(os.Args) > 2 {
        vflag := os.Args[2] 
        if vflag == "visio" {
            exp.Visio = true
        }
    }

    if err != nil  {
        fmt.Printf("drn2svg: error: %s", err.Error());
        os.Exit(3);
    }
    
    exp.Export(dra);
    exp.Export(dra);
    
    
    
    os.Exit(0);
}

